﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Rabbit.Sub
{
    internal class Program
    {
        private const int Iterations = 1000000;
        private static int _doshit = 1;
        private static long _pulled = 0;

        private static void Main(string[] args)
        {

            var watch = new Stopwatch();
            watch.Start();
            Parallel.Invoke(() => Consume("192.168.1.100"), () => Consume("192.168.1.101"), () => Consume("192.168.1.102"));
            watch.Stop();
            Console.WriteLine("Got it all in " + watch.Elapsed.Subtract(TimeSpan.FromMilliseconds(1000)).ToString());
            Console.ReadKey();

        }

        private static void Consume(string ip)
        {
            var factory = new ConnectionFactory {HostName = ip, UserName = "thedude", Password = "123"};
            using (var conn = factory.CreateConnection())
            using (var channel = conn.CreateModel())
            {
                channel.QueueDeclare("ololo", true, false, false, null);
                var consumer = new QueueingBasicConsumer(channel);
                channel.BasicConsume("ololo", false, consumer);
                while (_doshit > 0)
                {
                    BasicDeliverEventArgs result;
                    if (!consumer.Queue.Dequeue(1000, out result))
                    {
                        Interlocked.Exchange(ref _doshit, 0);
                        break;
                    }

                    var msg = result.Body;
                    long pulled = Interlocked.Increment(ref _pulled);
                    Console.WriteLine("Pulled " + pulled.ToString() + ", msg length: " + msg.Length.ToString());
                    channel.BasicAck(result.DeliveryTag, false);
                }
            }
        }
    }
}