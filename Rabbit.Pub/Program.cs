﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using RabbitMQ.Client;

namespace Rabbit.Pub
{
    public class Program
    {
        private const long MessageSize = 1024;
        private const int Iterations = 1000000;
        
        private static int _cnt = 0;
        private static long _pushed = 0;
        
        private static readonly Random _rnd = new Random();

        public static void Main(string[] args)
        {
            Parallel.Invoke(() => Write("192.168.1.100"), () => Write("192.168.1.101"), () => Write("192.168.1.102"));
        }

        private static void Write(string ip)
        {
            string num = "[" + Interlocked.Increment(ref _cnt).ToString() + "] ";
            var msg = GetRandomBytes();

            var factory = new ConnectionFactory { HostName = ip, UserName = "thedude", Password = "123" };
            using (var conn = factory.CreateConnection())
            using (var channel = conn.CreateModel())
            {
                channel.QueueDeclare("ololo", true, false, false, null);
                var props = channel.CreateBasicProperties();
                props.SetPersistent(true);

                var watch = new Stopwatch();
                watch.Start();
                do
                {
                    channel.BasicPublish("", "ololo", null, msg);
                    Console.WriteLine(num + "Pushed " + Interlocked.Read(ref _pushed).ToString());
                } while (Interlocked.Increment(ref _pushed) <= Iterations);
                watch.Stop();
                Console.WriteLine("Pushed all in " + watch.Elapsed.ToString());
                Console.ReadKey();
            }
        }

        private static byte[] GetRandomBytes()
        {
            var result = new byte[MessageSize];
            _rnd.NextBytes(result);

            return result;
        }
    }
}
