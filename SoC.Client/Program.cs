﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQClient;
using RestSharp;
using SoC.WebApi;

namespace SoC.Client
{
    class Program
    {
        private static readonly RestClient _client = new RestClient("http://not-limited.ru:2015");
        private static readonly Random _rnd = new Random();
        private static readonly int _threadCnt = Environment.ProcessorCount;

        private static IConnection _connection;
        private static IModel _channel;

        private static bool _maniac = false;
        private static long _cnt;
        private static Fir _fir20 = new Fir(20);
        private static Fir _fir1000 = new Fir(1000);
        private static Fir _firP = new Fir(20);

        static void Main(string[] args)
        {
            Console.WriteLine("Spoof of concert client.");
            Console.WriteLine("Format: [a] [b]\n'q' to exit, 'frenzy' to generate 10000 random requests, 'maniac' to generate requests in parallel until your house burns down.\n");
            InitRabbit();
            while (true)
            {
                Console.Write("> ");
                string input = Console.ReadLine();

                if (string.Equals(input, "q", StringComparison.OrdinalIgnoreCase))
                    return;

                if (string.Equals(input, "frenzy", StringComparison.OrdinalIgnoreCase))
                {
                    Frenzy();
                    continue;
                }

                if (string.Equals(input, "maniac", StringComparison.OrdinalIgnoreCase))
                {
                    Maniac();
                    continue;
                }

                var parts = input.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length != 2)
                {
                    Console.WriteLine("I need exactly 2 integers.");
                    continue;
                }

                int a, b;
                if (!int.TryParse(parts[0], out a) || !int.TryParse(parts[1], out b))
                {
                    Console.WriteLine("Better check yer numbers, dude!");
                    continue;
                }

                string response = Send(a, b);

                Console.WriteLine("Server says: " + response);
            }
        }

        private static void Maniac()
        {
            //CalibrateManiac();
            Console.WriteLine("Maniac mode. Press any key to start. Press any key again to stop.");
            Console.WriteLine("Will run " + _threadCnt + " maniacal threads.");
            Console.ReadKey();
            _maniac = true;
            _cnt = 0;
            
            RunManiac();

            Console.WriteLine("Maniac has been apprehended.");
        }

        private static void RunManiac()
        {
            var threads = new List<Thread>();
            for (int i = 0; i < _threadCnt; i++)
            {
                var thread = new Thread(ManiacThread);
                threads.Add(thread);
                thread.Start();
            }
            Console.ReadKey();
            _maniac = false;
            foreach (var thread in threads)
                thread.Join();
        }

        private static void ManiacThread()
        {
            while (_maniac)
            {
                long cnt = Interlocked.Increment(ref _cnt);
                SendRandom(cnt);
                Thread.Sleep(75);
            }
        }

        private static void Frenzy()
        {
            for (int i = 0; i <= 10000; i++)
                SendRandom(i);
        }

        private static void SendRandom(long i)
        {
            int a = _rnd.Next(10000), b = _rnd.Next(10000);
            string response = Send(a, b);
            Console.WriteLine("[" + i.ToString() + "] Sent " + a.ToString() + " + " + b.ToString() + " and got this: " + response);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static string Send(int a, int b)
        {
            string request = "api/Soc?a=" + a + "&b=" + b;
            return _client.Get(new RestRequest(request, Method.GET)).Content;
        }

        private static void InitRabbit()
        {
            var factory = new ConnectionFactory { HostName = "not-limited.ru", UserName = "thedude", Password = "123" };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare("outbox_queue", true, false, false, null);
            var props = _channel.CreateBasicProperties();
            props.SetPersistent(true);
            new Thread(RabbitThread).Start();
        }

        private static void RabbitThread()
        {
            var consumer = new QueueingBasicConsumer(_channel);
            _channel.BasicConsume("outbox_queue", false, consumer);
            
            while (true)
            {
                var result = consumer.Queue.Dequeue();
                if (result == null)
                    return;

                var now = DateTime.Now;
                _channel.BasicAck(result.DeliveryTag, false);
                var msg = result.Body;
                var stringMsg = Encoding.UTF8.GetString(msg);
                try
                {
                    var response = JsonConvert.DeserializeObject<OutboxMessage>(stringMsg);
                    //long rabbit1 = (long)response.Response.ProcessingStartTime.Subtract(response.Response.StartTime).TotalMilliseconds;
                    double processing = now.Subtract(response.Response.StartTime).TotalSeconds;
                    double mav20 = _fir20.FilterSample(processing);
                    double mav1000 = _fir1000.FilterSample(processing);
                    double mavP = _firP.FilterSample(response.Response.ProcessingEndTime.Subtract(response.Response.ProcessingStartTime).TotalSeconds);
                    //long rabbit2 = (long)now.Subtract(response.Response.ProcessingEndTime).TotalMilliseconds;
                    Console.WriteLine("Got response. Delta: {0:N2}, MAV(20): {1:N2}, MAV(1000): {2:N2}, MAV(P, 20): {3:N2}", processing, mav20, mav1000, mavP);
                }
                catch
                {
                    Console.WriteLine("Failed to deserialize message: " + stringMsg);
                }
            }
        }
    }
}
