﻿using System.Collections.Generic;

namespace SoC.Client
{
    public class Fir
    {
        private LinkedList<double> _buffer;
        private LinkedList<double> _coefficients;
        private double _filteredValue;

        public double FilteredValue { get { return _filteredValue; } }

        public Fir() //Default constructor: moving average, N=4
        {
            InitAvg(4);
        }

        public Fir(int order, double initialValue = 0.0d) //Constructor: moving average
        {
            InitAvg(order, initialValue);
        }


        public Fir(double[] coefficients, double initialBufferValue = 0.0d) //Constructor: Fir
        {
            _buffer = new LinkedList<double>();
            _coefficients = new LinkedList<double>();
            for (int i = 0; i < coefficients.Length; i++)
            {
                _coefficients.AddLast(coefficients[i]);
                _buffer.AddLast(initialBufferValue);
            }
        }

        private void InitAvg(int order, double initialValue = 0.0d)
        {
            _buffer = new LinkedList<double>();
            _coefficients = new LinkedList<double>();
            double coefficient = 1.0d / (order);
            for (int i = 0; i < order; i++)
            {
                _coefficients.AddLast(coefficient);
                _buffer.AddLast(initialValue);
            }
        }

        public double FilterSample(double sample)
        {
            double output = 0;
            _buffer.AddFirst(sample);
            _buffer.RemoveLast();
            using (var bufEnum = _buffer.GetEnumerator())
            {
                foreach (var coefficient in _coefficients)
                {
                    bufEnum.MoveNext();
                    output += coefficient * bufEnum.Current;
                }
            }
            return output;
        }
    }
}