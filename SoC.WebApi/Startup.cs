﻿using System.Web.Http;
using Owin;

namespace SoC.WebApi
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}",
                defaults: new {  }
            );

            appBuilder.UseWebApi(config);
        } 
    }
}