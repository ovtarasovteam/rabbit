﻿using System;
using System.Text;
using System.Threading;
using System.Web.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RabbitMQ.Client;
using RabbitMQClient;

namespace SoC.WebApi
{
    public class SocController : ApiController
    {
        private static IConnection _connection;
        private static IModel _channel;

        static SocController()
        {
            var factory = new ConnectionFactory {HostName = "not-limited.ru", UserName = "thedude", Password = "123"};
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare("inbox_queue", true, false, false, null);
            _channel.ExchangeDeclare("inbox_exchange", "fanout", true);
            _channel.QueueBind("inbox_queue", "inbox_exchange", "");
            var props = _channel.CreateBasicProperties();
            props.SetPersistent(true);
        }

        public string Get(int a, int b)
        {
            var request = CreateRequest(a, b);
            return TrySend(request);
        }

        public static InboxMessage CreateRequest(int a, int b)
        {
            var request = new InboxMessage
                          {
                              ProcessCode = "Calculator",
                              Request = new InboxRequest
                                        {
                                            A = a,
                                            B = b,
                                            StartTime = DateTime.Now,
                                            Identifier = Guid.NewGuid()
                                        }
                          };
            return request;
        }

        private string TrySend(InboxMessage request)
        {
            try
            {
                Console.WriteLine("Sending " + request.Request.A.ToString() + " + " + request.Request.B.ToString() + " with id: " + request.Request.Identifier);
                Push(GetRequest(request));
                return request.Request.Identifier.ToString("N");
            }
            catch
            {
                return "ERROR: The rabbit is dead.";
            }
        }

        public static byte[] GetRequest(object request)
        {
            string json = JObject.FromObject(request).ToString(Formatting.Indented);
            return Encoding.UTF8.GetBytes(json);
        }

        public static void Push(byte[] message)
        {
            _channel.BasicPublish("inbox_exchange", "", null, message);
        }
    }
}