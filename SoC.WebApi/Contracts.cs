﻿using System;
using Newtonsoft.Json;

namespace RabbitMQClient
{
    public class InboxMessage
    {
        [JsonProperty("processCode")]
        public string ProcessCode { get; set; }

        [JsonProperty("request")]
        public InboxRequest Request { get; set; }
    }

    public class InboxRequest
    {
        [JsonProperty("startTime")]
        public DateTime StartTime { get; set; }

        [JsonProperty("a")]
        public int A { get; set; }

        [JsonProperty("b")]
        public int B { get; set; }

        [JsonProperty("identifier")]
        public Guid Identifier { get; set; }
    }

    public class OutboxMessage
    {
        [JsonProperty("processCode")]
        public string ProcessCode { get; set; }

        [JsonProperty("pid")]
        public string Pid { get; set; }

        [JsonProperty("response")]
        public OutboxResponse Response { get; set; }
    }

    public class OutboxResponse
    {
        [JsonProperty("startTime")]
        public DateTime StartTime { get; set; }

        [JsonProperty("processingStartTime")]
        public DateTime ProcessingStartTime { get; set; }

        [JsonProperty("processingEndTime")]
        public DateTime ProcessingEndTime { get; set; }

        [JsonProperty("a")]
        public int A { get; set; }

        [JsonProperty("b")]
        public int B { get; set; }

        [JsonProperty("result")]
        public int Result { get; set; }

        [JsonProperty("identifier")]
        public Guid Identifier { get; set; }
    }
}
